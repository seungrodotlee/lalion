import { computed, watch } from "vue";
import { useStore } from "vuex";

export default function useTopbar() {
  const store = useStore();
  const currentMenu = computed(() => {
    return store.getters["getCurrentMenu"];
  });
  const componentsHeightList = computed(() => {
    return store.getters["getComponentsHeightList"];
  });

  const scrollToSection = async (section, menu) => {
    await store.dispatch("setCurrentMenu", menu.value).then((res) => {
      section.scrollIntoView({ behavior: "smooth" });
    });
  };

  const chaseYPosition = (componentsHeightList) => {
    if (!componentsHeightList) return;
    let yPosition = 0,
      heightList = [];
    Object.keys(componentsHeightList).forEach((el) => {
      yPosition += componentsHeightList[el];
      heightList.push({ name: el, height: yPosition });
    });

    heightList.forEach(async (el, curIdx) => {
      if (
        heightList[curIdx - 1] &&
        (heightList[curIdx - 1] && heightList[curIdx - 1].height) <=
          window.scrollY &&
        window.scrollY < el.height
      ) {
        if (currentMenu.value !== el.name) {
          await store.dispatch("setCurrentMenu", el.name);
          return;
        }
      } else if (window.scrollY < heightList[0].height) {
        if (currentMenu.value !== "home") {
          await store.dispatch("setCurrentMenu", el.name);
          return;
        }
      }
    });
  };

  return { scrollToSection, chaseYPosition };
}
