import { computed } from "vue";
import { useStore } from "vuex";

export default function useScroll() {
  const store = useStore;
  const currentMenu = computed(() => {
    return store.getters["getCurrentMenu"];
  });
  let scroll =
    window.requestAnimationFrame ||
    // IE Fallback
    function (callback) {
      window.setTimeout(callback, 1000 / 60);
    };
  let elementsToShow = document.querySelectorAll(".show-on-scroll");

  function loop() {
    console.log("123123123: ", elementsToShow);
    Array.prototype.forEach.call(elementsToShow, function (element) {
      if (isElementInViewport(element)) {
        element.classList.add("is-visible");
      } else {
        element.classList.remove("is-visible");
      }
    });

    scroll(loop);
  }

  // loop();

  function isElementInViewport(el) {
    console.log("el2323: ", el);
    // special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
      el = el[0];
    }
    let rect = el.getBoundingClientRect();
    return (
      (rect.top <= 0 && rect.bottom >= 0) ||
      (rect.bottom >=
        (window.innerHeight || document.documentElement.clientHeight) &&
        rect.top <=
          (window.innerHeight || document.documentElement.clientHeight)) ||
      (rect.top >= 0 &&
        rect.bottom <=
          (window.innerHeight || document.documentElement.clientHeight))
    );
  }

  return { loop };
}
