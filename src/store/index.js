import { createStore } from "vuex";

export default createStore({
  state: {
    currentMenu: "home",
    componentsHeightList: [],
  },
  getters: {
    getCurrentMenu(state) {
      return state.currentMenu;
    },
    getComponentsHeightList(state) {
      return state.componentsHeightList;
    },
  },
  mutations: {
    setCurrentMenu(state, payload) {
      state.currentMenu = payload;
    },
    setComponentsHeightList(state, payload) {
      payload.forEach((el) => {
        if (el) {
          state.componentsHeightList[el.name] = el.height;
        }
      });
    },
  },
  actions: {
    setCurrentMenu({ commit, rootState }, payload) {
      return new Promise(async (resolve, reject) => {
        commit("setCurrentMenu", payload);
        resolve(rootState.currentMenu);
      });
    },
    setComponentsHeight({ commit, rootState }, payload) {
      return new Promise(async (resolve, reject) => {
        commit("setComponentsHeightList", payload);
        resolve(rootState.componentsHeightList);
      });
    },
  },
});
