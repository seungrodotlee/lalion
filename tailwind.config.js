module.exports = {
  mode: "jit",
  purge: ["./src/**/*.{js,vue}"],
  darkMode: false,
  theme: {
    screens: {
      desktop: "1280px",
      laptop: { max: "1035px" },
      laptop_md: "1241px",
      laptop_min: "981px",
      tablet: { max: "980px" },
      tablet_md: { max: "815px" },
      tablet_sm: { max: "768px" },
      mobile_lg_min: "426px",
      mobile_lg: { max: "425px" },
      mobile_sm: { max: "400px" },
    },
  },
  plugins: [],
};
